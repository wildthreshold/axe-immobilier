<?php

namespace Drupal\axe\EventSubscriber;

use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber subscribing to KernelEvents::REQUEST.
 */
class RedirectAnonymousSubscriber implements EventSubscriberInterface {


    public static function getSubscribedEvents() {
         $events[KernelEvents::REQUEST][] = ['checkAuthStatus'];
         return $events;
    }


    public function checkAuthStatus(RequestEvent $event) {

        $current_path = \Drupal::service('path.current')->getPath();

        $loginUrl = Url::fromRoute('user.login')->toString();
        $routeName = \Drupal::routeMatch()->getRouteName();


        if (
            \Drupal::currentUser()->isAnonymous() &&
//            $routeName != 'user.login' &&
//            $routeName != 'user.reset' &&
//            $routeName != 'user.reset.form' &&
//            $routeName != 'user.reset.login' &&
//            $routeName != 'user.pass'

            // $routeName == 'system.admin' &&
//            $routeName == 'system.403'
            $current_path == '/axe-admin'
        ) {
            // Add logic to check other routes you want available to anonymous users,
            // otherwise, redirect to login page.
            if (strpos($routeName, 'view') === 0 && strpos($routeName, 'rest_') !== FALSE) {
                return;
            }

            $response = new RedirectResponse($loginUrl, 301);
            $response->send();
        }
    }

}

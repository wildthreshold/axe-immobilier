<?php

namespace Drupal\axe\Controller;

use Drupal\Core\Controller\ControllerBase;



class AxeController extends ControllerBase {


    public function accueil() {
        return [
//            '#theme' => 'realisations'
            '#type' => 'markup',
        ];
    }


    /**
     * Returns a page title.
     */
//    public function getTitle() {
//
//        $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
//        switch($language) {
//            case 'en': $title = 'Wo are we ?'; break;
//            case 'fr': $title = 'Qui sommes nous ?'; break;
//        }
//        return  $title;
//    }


    public function qui_sommes_nous() {
        return [ '#type' => 'markup' ];
    }
    public function qui_sommes_nous_expertises() {
        return [ '#type' => 'markup' ];
    }
    public function qui_sommes_nous_equipe() {
        return [ '#type' => 'markup' ];
    }
//    public function qui_sommes_nous_comite_executif() {
//        return [ '#type' => 'markup' ];
//    }


    public function realisations() {
        return [ '#type' => 'markup' ];
    }






    public function test_page($from, $to) {
        return [
            '#theme' => 'test_page',
            '#from' => $from,
            '#to' => $to,
        ];
    }



    public function content() {
        return [
          '#theme' => 'test',
          '#test_var' => $this->t('Ma variable custom'),
        ];
    }


}

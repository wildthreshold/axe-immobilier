<?php
namespace Drupal\axe\TwigExtension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;


class TwigFunctionExtension extends AbstractExtension {

    /**
     * Declare your custom twig extension here
     * @return array|\Twig_SimpleFunction[]
     */
    public function getFunctions() {
        return [
            new TwigFunction('display_block_by_id',
                [ $this, 'display_block' ],
                [ 'is_safe' => ['html'] ]
            )
        ];
    }

    /**
     * Function to get and render block by id
     * @param $block_id
     *  Block id to render
     *
     * @return array
     */
    public function display_block($block_id) {
        $block = \Drupal\block\Entity\Block::load($block_id);
        return \Drupal::entityTypeManager()
            ->getViewBuilder('block')
            ->view($block);
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getName() {
        return 'twig_extension.function';
    }
}
<?php
namespace Drupal\axe\TwigExtension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Drupal\axe\Utils\UtilsFunction;


class TwigFilterExtension extends AbstractExtension {

    /**
     * Declare your custom twig filter here
     * @return array|\Twig_SimpleFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('replace_wth_x', [$this, 'replaceWithX']),
            new TwigFilter('remove_accent', [$this, 'removeAccent'])
        ];
    }

    public function replaceWithX($text) {
        $text = preg_replace('/[^\s]/', 'x', $text);
        return $text;
    }

    public function removeAccent($text) {
        return UtilsFunction::unaccent($text);
    }

    /**
     * {@inheritdoc}
     * @return string
     */
    public function getName()
    {
        return 'twig_extension.filter';
    }
}
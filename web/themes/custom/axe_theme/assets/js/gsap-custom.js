/**
 * Petite fonction util qui change le backgroud d'un élement.
 * @param bElem
 * @param bUrl
 * @returns {string}
 */
const changeBackground = (bg_elem, new_bg) => {
  bg_elem.style.background = `url("${new_bg}") center center / cover no-repeat`;
};
window.addEventListener("load", function () {

  // Hover lien Réalisation Master ******************************************************
  let gridRealisationItem = document.querySelectorAll(
    ".grid-realisations__item"
  );
  if (gridRealisationItem) {

    gridRealisationItem.forEach((c) => {
      let tl = gsap.timeline();
      c.addEventListener("mouseenter", () => {
        tl.play();
      });
      c.addEventListener("mouseleave", () => {
        tl.reverse();
      });
      c.addEventListener("click", () => {
        tl.play();
      });
    });

  }

});

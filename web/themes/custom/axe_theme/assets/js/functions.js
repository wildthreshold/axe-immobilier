// @VARS
const supportsTouch = "ontouchstart" in window || navigator.msMaxTouchPoints;
const theme_path = "/" + drupalSettings.path.theme_path;
let menuAfter = document.querySelector(".menu-after");
let addNav = document.querySelectorAll(".addnav");

// Lottie loader ******************************************************
let animationLoader = bodymovin.loadAnimation({
    container: document.getElementById("lottieLoader"),
    path: theme_path + "/assets/images/logoReverse.json",
    renderer: "canvas",
    loop: false,
    autoplay: false,
});
let animationPreloader = bodymovin.loadAnimation({
    container: document.getElementById("lottiePreloader"),
    path: theme_path + "/assets/images/logo.json",
    renderer: "canvas",
    loop: false,
    autoplay: false,
});

// Lottie curseur
let curseurAnim = bodymovin.loadAnimation({
    container: document.getElementById("curseurAnim"),
    path: drupalSettings.path.currentLanguage === "fr" ? theme_path + "/assets/images/curseur_master.json" : theme_path + "/assets/images/curseur_master_EN.json",
    renderer: "canvas",
    loop: false,
    autoplay: false,
});

// Anim bouton back
function animBackbouton() {
    let backBtns = document.querySelectorAll(".grid-back,.retour");
    backBtns.forEach((backBtn) => {
        animbtnBack = gsap.timeline();
        animbtnBack.from(backBtn, {
            y: 100,
            autoAlpha: 0,
            delay: 1,
            ease: "expo.out",
        });
        if (backBtn) {
            animbtnBack.play().timeScale(0.75);
        }
    });
}

// Lottie 404 ******************************************************
function anim404() {
    let animation404 = bodymovin.loadAnimation({
        container: document.getElementById("anim404"),
        path: theme_path + "/assets/images/error.json",
        renderer: "canvas",
        loop: true,
        autoplay: true,
    });
    let is404 = document.querySelector("#anim404");
    if (is404) {
        animation404.play();
    }
}

// Mosaique Equipe ******************************************************
function tailleItemsMosaique() {
    let vignetteEquipier = document.querySelector(".moisaique-profil article");
    let logo = document.querySelector(".logo-axe");
    if (vignetteEquipier) {
        // L'image de gauche fera la hauteur de 2 vignettes
        let vignetteEquipierHeight = vignetteEquipier.offsetHeight;
        let imgPrincipale = document.querySelector(".img-principal");
        imgPrincipale.style.height = vignetteEquipierHeight * 2 + 2 + "px";
        // logo Axe bas de page
        if (logo) {
            logo.style.height = vignetteEquipierHeight + "px";
        }
    }
}

// Anim news Accueil ******************************************************
function animNews() {
    let derniersArticles = document.querySelector(".derniers-articles");
    if (derniersArticles) {
        let tl = gsap.timeline();
        tl.from(".derniers-articles--desktop .views-row", {
            y: 70,
            autoAlpha: 0,
            duration: 1,
            ease: "expo",
            stagger: 0.1,
            delay: 0.33,
        });
    }
}

// Effet Kenburns ******************************************************
function kenburns() {
    let kenburn = document.querySelector(".kenburns");
    if (kenburn) {
        let tl = gsap.timeline();
        tl.from(".kenburns", {
            scale: 1.3,
            duration: 14,
            ease: "none",
        });
    }
}

// Taille banniere Accueil
function hauteurBanniereAccueil() {
    let banniereAccueil = document.querySelector(".header-home");
    if (banniereAccueil) {
        if (window.innerWidth > 1199) {
            let navbarHeight = document.querySelector(".menu-desktop").offsetHeight;
            banniereAccueil.style.height =
                window.innerHeight - navbarHeight + 1 + "px";
        }
        else {
        }
    }
}

// LOCOMOTIVE SCROLL ******************************************************
let locoScroll = new LocomotiveScroll({
    el: document.querySelector(".smooth"),
    // smooth: true,
    // lerp: 0.25,
});

// Anims barres Projet full
// ******************************************************
function animBarres() {
    // Apparition des barres
    const barres = document.querySelectorAll(".barre");
    if (barres) {
        gsap.to(".barre", {
            clipPath: "inset(0% 0% 0% 0%)",
            stagger: 0.175,
            duration: 0.9,
            ease: "expo.inOut",
        });
    }

    // Chiffres
    const counters = document.querySelectorAll(".animchiffre");
    const speed = 300;

    counters.forEach((counter) => {
        const animate = () => {
            const value = +counter.getAttribute("data-value");
            const data = +counter.innerText;
            const time = value / speed;
            if (data < value) {
                counter.innerText = Math.ceil(data + time);
                setTimeout(animate, 1);
            }
            else {
                counter.innerText = value;
            }
        };
        animate();
    });
}

locoScroll.on("call", () => {
    animBarres();
});

// Grid Réalisations et div carrées
// ******************************************************
function squareSize() {
    // DESKTOP
    if (window.innerWidth > 1199) {
        let realisationsSquare = document.querySelectorAll(
            ".grid-realisations .views-row,.ctas__inner,.triptique__inner,.realisations__item"
        );
        realisationsSquare.forEach((square) => {
            square.style.height = square.getBoundingClientRect().width + "px";
        });
    }
    // MOBILE
    else {
        let realisationsSquare = document.querySelectorAll(
            ".grid-realisations .views-row,.ctas__inner,.realisations__item,.bg-header.kenburns,.carte-profil,.img-principal"
        );
        realisationsSquare.forEach((square) => {
            square.style.height = square.getBoundingClientRect().width + "px";
        });
    }
    // bg accueil bottom mobile
    if (window.innerWidth < 768) {
        let bgHeaderBottom = document.querySelector(".bg-header--bottom");
        if (bgHeaderBottom) {
            bgHeaderBottom.style.height =
                bgHeaderBottom.getBoundingClientRect().width / 2 + "px";
        }
    }
}

/**
 * beforeunload :
 */
window.addEventListener("beforeunload", function (e) {
    document.querySelector(".preloader").classList.add("visible");
    //animationPreloader.pause();
});

/**
 * load :
 * Tous les éléments HTML sont chargé et construit ainsi que toute les
 * subframes (img, css, script, etc...)
 */
window.addEventListener("load", function () {
    // Curseur ******************************************************
    let $follow = document.querySelector(".circle-follow");
    let noHover = document.querySelectorAll(".nohoverlink,.link-text");
    let hoverImages = document.querySelectorAll(
        ".bg-header,.img-principale,.bg-vertical"
    );

    function moveCircle(e) {
        gsap.to($follow, {
            x: e.clientX,
            y: e.clientY,
            duration: 0.4,
            force3D: false,
        });
    }

    /** ************************************************************************
     *  Curseur
     * *******************************************************************
     ************************************************************************ */

    if (window.innerWidth > 1199) {
        // Init curseur
        setTimeout(() => {
            curseurAnim.setSpeed(1);
            curseurAnim.playSegments([0, 4], true);
            document.body.addEventListener("mousemove", moveCircle);
        }, 100);

        hoverImages.forEach((link) => {
            link.addEventListener("mouseout", (event) => {
                curseurAnim.setSpeed(1);
                curseurAnim.playSegments([0, 4], true);
            });
        });

        document.querySelectorAll(".curseurfull").forEach((curseur) => {
            curseur.addEventListener("mouseover", (event) => {
                curseurAnim.setSpeed(0.4);
                curseurAnim.playSegments([9, 40], true);
            });
        });
    }

    // Apparition de .contenu pour anims d'intro plus smooths
    // ******************************************************
    let contenu = document.querySelector(".contenu");
    if (contenu) {
        document.querySelector(".contenu").style.visibility = "visible";
    }

    // toggle Recherche ******************************************************
    let addNav = document.querySelectorAll(".addnav");
    document.querySelectorAll(".recherche-but").forEach((item) => {
        item.addEventListener("click", (e) => {
            addNav.forEach((el) => {
                el.classList.contains("visible")
                    ? el.classList.remove("visible")
                    : el.classList.add("visible");
            });
        });
    });

    // Banniere Accueil ******************************************************
    hauteurBanniereAccueil();

    // Mosaique Equipes ******************************************************
    tailleItemsMosaique();

    // Placements elements sous la navbar
    // ******************************************************
    let navbarHeight = document.querySelector(".menu-desktop").offsetHeight;
    let subNavs = document.querySelectorAll("ul.mp-items-submenu");
    subNavs.forEach((subNav) => {
        subNav.style.top = navbarHeight + "px";
    });

    // position Recherche
    // ***************************************************************
    addNav.forEach((nav) => {
        window.matchMedia("(min-width: 992px)")
            ? nav.style.top = "84px"
            : nav.style.top = navbarHeight + "px";
    });

    // position Menu after
    // ***************************************************************
    if (menuAfter) {
        menuAfter.style.top = navbarHeight + "px";
    }

    // Grid Réalisations ******************************************************
    squareSize();

    // Loader ******************************************************
    let loader = document.querySelector(".loader");
    if (loader) {
        animationLoader.play();
    }

    // PAGE GRID REALLISATIONS
    // ******************************************************
    let viewRow = document.querySelector(".views-row");
    if (viewRow) {
        gsap.to(".views-row", {
            clipPath: "inset(0% 0% 0% 0%)",
            stagger: 0.175,
            duration: 0.6,
            ease: "expo",
        });
    }

    // LOADER ******************************************************
    if (loader) {
        loader.classList.add("loaderLoaded");
    }

    // UPDATE LOCOSCROLL ******************************************************
    locoScroll.update();

    // ANIM LINES ******************************************************
    const animation_cb_qui_sommes_nous = () => {
        let animLines = gsap.timeline({paused: true});
        // titre page
        animLines.from(
            "h1",
            {
                autoAlpha: 0,
                y: 50,
                duration: 0.2,
                ease: "circ.inOut",
                transformOrigin: "left",
            },
            0.01
        );
        // Sous-titre
        animLines.from(
            ".anim-p",
            {
                autoAlpha: 0,
                y: 50,
                duration: 0.2,
                ease: "circ.inOut",
                transformOrigin: "left",
            },
            0.01
        );
        // Border
        animLines.from(
            ".key-bottom",
            {
                scaleX: 0,
                duration: 0.2,
                ease: "circ.inOut",
                transformOrigin: "left",
            },
            0.01
        );
        // Intro
        animLines.from(
            ".intro *",
            {
                autoAlpha: 0,
                x: -50,
                duration: 0.3,
                ease: "expo.out",
                transformOrigin: "left",
                stagger: 0.01,
            },
            0.1
        );
        // Triptique
        animLines.from(
            ".key-top--triptique",
            {
                scaleX: 0,
                duration: 0.2,
                ease: "circ.inOut",
                transformOrigin: "left",
            },
            0.01
        );
        animLines.from(
            ".triptique__inner ",
            {
                autoAlpha: 0,
                x: -50,
                duration: 0.3,
                ease: "expo.out",
                transformOrigin: "left",
                stagger: 0.01,
            },
            0.1
        );
        // Contenu cards
        animLines.from(
            ".key-top--ctas",
            {
                scaleX: 0,
                duration: 0.2,
                ease: "circ.inOut",
                transformOrigin: "left",
            },
            0.01
        );
        animLines.from(
            ".ctas__inner",
            {
                autoAlpha: 0,
                x: -50,
                duration: 0.3,
                ease: "expo.out",
                transformOrigin: "left",
            },
            0.2
        );
        // Go!
        animLines.play().timeScale(0.25);
    };
    // ANIM LINES ENGAGEMENTS ETC ..
    // ******************************************************
    const animation_cartes_teaser = () => {
        let animLines = gsap.timeline({paused: true});
        let cards = document.querySelector(".ctas__inner");
        if (cards) {
            let cardWidth = document.querySelector(".ctas__inner").offsetWidth;
            // titre page
            animLines.from(
                "h1",
                {
                    autoAlpha: 0,
                    y: 50,
                    duration: 0.2,
                    ease: "circ.inOut",
                    transformOrigin: "left",
                },
                0.01
            );
            if (document.querySelector(".anim-p") != null) {
                animLines.from(
                    ".anim-p",
                    {
                        autoAlpha: 0,
                        y: 50,
                        duration: 0.2,
                        ease: "circ.inOut",
                        transformOrigin: "left",
                    },
                    0.01
                );
            }
            ///// 1 LEFT + RIGHT
            animLines.from(
                ".key-center--left",
                {x: cardWidth - 1, duration: 0.2, ease: "circ.inOut"},
                0.01
            );
            animLines.from(
                ".key-center--right",
                {x: -(cardWidth - 2), duration: 0.2, ease: "circ.inOut"},
                0.01
            );
            animLines.to(
                ".key-center--left",
                {scaleY: 1, duration: 0.2, ease: "circ.inOut"},
                0.02
            );
            animLines.to(
                ".key-center--right",
                {scaleY: 1, duration: 0.2, ease: "circ.inOut"},
                0.02
            );
            ///// 2 CENTER
            animLines.to(
                ".key-center",
                {scaleY: 1, duration: 0.275, ease: "circ.inOut"},
                -0.01
            );
            /// 3 Top LEFT + TOP RIGHT
            animLines.to(
                ".key-top--left",
                {
                    scaleX: 1,
                    duration: 0.2,
                    ease: "circ.inOut",
                    transformOrigin: "right",
                },
                0.04
            );
            animLines.to(
                ".key-top--right",
                {
                    scaleX: 1,
                    duration: 0.2,
                    ease: "circ.inOut",
                    transformOrigin: "left",
                },
                0.04
            );
            // Contenu cards
            animLines.from(
                ".ctas__inner",
                {
                    autoAlpha: 0,
                    x: -50,
                    duration: 0.3,
                    ease: "expo.out",
                    transformOrigin: "left",
                },
                0.2
            );
            // Go!
            animLines.play().timeScale(0.25);
        }
    };
    // REALISATION GRID ******************************************************
    const anims_realisations = () => {
        document.querySelectorAll(".grid-realisations__item").forEach((element) => {
            element.addEventListener("click", (event) => {
                event.preventDefault();
                let targeted_link = element.getElementsByTagName("a");
                if (targeted_link[0].href) {
                    let target = targeted_link[0].href;
                    setTimeout(() => (window.location = target), 1000);
                }

                // append le bg de la carte cliquée à toutes les cells
                let thisBg = element.querySelector(".image").style.backgroundImage;

                let thisLink = element.querySelector("a").getAttribute("href");

                function getLink() {
                    window.location.href = thisLink;
                }

                let currentBgUri = thisBg.replace(/(url\(|\)|")/g, "").split(" ")[0];

                let image = document.querySelectorAll(
                    ".grid-realisations__item  .image"
                );

                let img_back = document.querySelectorAll(".image-back");
                let overlays = document.querySelectorAll(".overlay");
                let links = document.querySelectorAll('.grid-realisations__item a');

                img_back.forEach((img) => {
                    // on recup la position X et Y de chaque cell préalablement
                    // créée
                    let pos = img.getClientRects()[0];
                    let left = pos.left;
                    let top = pos.top;
                    gsap.set(img, {
                        backgroundImage: "url(" + currentBgUri + ")",
                        backgroundPosition: -left + "px " + -top + "px",
                        backgroundSize: window.innerWidth + "px",
                        rotation: 0.1,
                    });
                });

                img_back.forEach((img) => {
                    gsap.to(img, {
                        webkitClipPath: "inset(0% 0% 0% 0%)",
                        clipPath: "inset(0% 0% 0% 0%)",
                        stagger: 0.175,
                        duration: 0.9,
                        ease: "power4",
                        rotation: 0.1,
                        force3D: true,
                    });
                });

                overlays.forEach((img) => {
                    gsap.to(img, {
                        autoAlpha: 0,
                        stagger: 0.175,
                        duration: 0.9,
                        ease: "power4",
                        rotation: 0.1,
                        force3D: true,
                    });
                });

                links.forEach((img) => {
                    gsap.to(img, {
                        autoAlpha: 0,
                        stagger: 0.175,
                        duration: 0.9,
                        ease: "power4",
                        rotation: 0.1,
                        force3D: true,
                    });
                });

            });


        });
    };
    if (document.querySelector(".custom-block-qui-sommes-nous") != null) {
        animation_cb_qui_sommes_nous();
    }
    else if (document.querySelector(".realisations") != null) {
        anims_realisations();
    }
    else {
        animation_cartes_teaser();
    }

    // 404 ******************************************************
    anim404();

    // Bouton retour ******************************************************
    animBackbouton();

    // Anim news accueil ******************************************************
    animNews();

    // Effet Kenburns ******************************************************
    kenburns();

    /** ************************************************************************
     *  MENU HOVER OVERLAY NOIR
     * *******************************************************************
     ************************************************************************ */
    let menuItems = document.querySelectorAll(".menu-item--expanded");
    menuItems.forEach((menuItem) => {
        if (window.innerWidth > 1199) {
            let menuAfter = document.querySelector(".menu-after");
            menuItem.addEventListener("mouseenter", () => {
                menuAfter.classList.add("visible");
            });
            menuItem.addEventListener("mouseleave", () => {
                menuAfter.classList.remove("visible");
            });
        }
    });
    /** ************************************************************************
     *  MENU MOBILE
     * *******************************************************************
     ************************************************************************ */
    const link_menu_expanded = document.querySelectorAll(".menu-mobile li.menu-item--expanded > a");
    const link_svg_menu_expanded = document.querySelectorAll(".menu-mobile li.menu-item--expanded > a > svg");
    link_svg_menu_expanded.forEach((item) => {
        item.addEventListener("click", (e) => {
            e.preventDefault();
            const sous_menu = item.parentNode.nextElementSibling;
            if (!sous_menu.classList.contains("slideDown")) {
                sous_menu.classList.add("slideDown");
                item.style.transform = "rotate(180deg)";
            }
            else {
                sous_menu.classList.remove("slideDown");
                item.style.transform = "rotate(0)";
            }
        });
    });

    const burger_btn = document.querySelector(".hamburger.hamburger--emphatic");
    const sous_menu_mobile = document.querySelector(".menu-mobile nav > ul.mp-items");
    burger_btn.addEventListener("click", (e) => {
        e.preventDefault();
        if (!burger_btn.classList.contains("is-active")) {
            burger_btn.classList.add("is-active");
            sous_menu_mobile.classList.add("slideDown");
            if (window.innerWidth < 1199) {
                document.querySelector("#block-selecteurdelangue").style.height = "44px";
                document.querySelector("#block-selecteurdelangue").style.opacity = 1;
            }
        }
        else {
            burger_btn.classList.remove("is-active");
            sous_menu_mobile.classList.remove("slideDown");
            if (window.innerWidth < 1199) {
                document.querySelector("#block-selecteurdelangue").style.height = 0;
                document.querySelector("#block-selecteurdelangue").style.opacity = 0;
            }
        }
    });
});
/**
 * DOMContentLoaded :
 * Tous les éléments HTML sont chargé et construit
 */
document.addEventListener("DOMContentLoaded", () => {
});
/**
 * Resize :
 */
window.addEventListener("resize", function (event) {
    // Banniere Accueil
    hauteurBanniereAccueil();
    // Mosaique Equipes
    squareSize();
    // Update scroll
    locoScroll.update();
    // Image de gauche page Equipe détail
    tailleItemsMosaique();
    // Placements elements sous la navbar
    let navbarHeight = document.querySelector(".menu-desktop").offsetHeight;
    let subNavs = document.querySelectorAll("ul.mp-items-submenu");
    subNavs.forEach((subNav) => {
        subNav.style.top = navbarHeight + "px";
    });
    // position Recherche
    // ***************************************************************
    addNav.forEach((nav) => {
        nav.style.top = navbarHeight + "px";
    });
    // position Menu after
    // ***************************************************************
    if (menuAfter) {
        menuAfter.style.top = navbarHeight + "px";
    }
});




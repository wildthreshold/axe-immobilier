<?php


function axe_theme_theme_suggestions_alter(array &$suggestions, array $variables, $hook) {
    if ($hook == 'form' & !empty($variables['element']['#id'])) {
        $suggestions[] = 'form__' . str_replace('-', '_', $variables['element']['#id']);
    }
}


/**
 * Modification des suggestions de pages
 */
function axe_theme_theme_suggestions_page_alter(array &$suggestions) {
//    $route = \Drupal::routeMatch()->getRouteObject();
//    $route_node = \Drupal::routeMatch()->getParameter('node');
//    $view_id = $route->getDefault('view_id');
//    $display_id = $route->getDefault('display_id');

    if ($node = \Drupal::routeMatch()->getParameter('node')) {
        $content_type = $node->bundle();
        $suggestions[] = 'page__' . $content_type;
        if ( $node->getType() == "page" ) {
            $pathauto_url = \Drupal::service('pathauto.alias_cleaner')->cleanString($node->title->value);
            $suggestions[] = 'page__' . str_replace("-", "_", $pathauto_url);
        }
    }
    if ($node = \Drupal::routeMatch()->getParameter('taxonomy_term')) {
        $vocabulaire = $node->bundle();
        $term = strtolower($node->name->value);
        $suggestions[] = 'page__taxonomy__' . $vocabulaire;
        $suggestions[] = 'page__taxonomy__' . $vocabulaire . '_' . str_replace(" ", "_", $term);
    }
}


/**
 * Modification des suggestions de block
 * Implements hook_theme_suggestions_HOOK_alter() for form templates.
 */
function axe_theme_theme_suggestions_block_alter(array &$suggestions, array $variables) {
    // Block suggestions for custom block bundles.
    if (isset($variables['elements']['content']['#block_content'])) {
        array_splice($suggestions, 1, 0, 'block__bundle__' . $variables['elements']['content']['#block_content']->bundle());
        //        $suggestions[] = 'block__' . $variables['elements']['content']['#block_content']->bundle();
    }
}


/**
 * Implements hook_theme_suggestions_views_view_alter().
 */
function axe_theme_theme_suggestions_views_view_alter(array &$suggestions, array $variables) {
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
        if (isset($variables['view']->element['#display_id'])) {
            $suggestions[] = 'views_view__' . $variables['view']->element['#display_id'];
        }
    }
    if ($node = \Drupal::routeMatch()->getParameter('taxonomy_term')) {
        $vocabulaire = $node->bundle();
        $term = strtolower($node->name->value);
        $suggestions[] = 'views_view__' . $vocabulaire;
        $suggestions[] = 'views_view__' . $vocabulaire . '_' . $term;
    }
    if (isset($variables['view']->element['#display_id'])) {
        $suggestions[] = 'views_view__' . $variables["view"]->element["#name"];
        $suggestions[] = 'views_view__' . $variables['view']->element['#display_id'];
    }
}

/**
 * Implements hook_theme_suggestions_views_view_unformatted_alter().
 */
function axe_theme_theme_suggestions_views_view_unformatted_alter(array &$suggestions, array $variables) {
    if ($node = \Drupal::routeMatch()->getParameter('node')) {
        if (isset($variables['view']->element['#display_id'])) {
            $suggestions[] = 'views_view_unformatted__' . $variables['view']->element['#display_id'];
        }
    }
    if ($node = \Drupal::routeMatch()->getParameter('taxonomy_term')) {
        $vocabulaire = $node->bundle();
        $term = strtolower($node->name->value);
        $suggestions[] = 'views_view_unformatted__' . $vocabulaire;
        $suggestions[] = 'views_view_unformatted__' . $vocabulaire . '_' . $term;
    }
    if (isset($variables['view']->element['#display_id'])) {
        $suggestions[] = 'views_view_unformatted__' . $variables["view"]->element["#name"];
        $suggestions[] = 'views_view_unformatted__' . $variables['view']->element['#display_id'];
    }
}

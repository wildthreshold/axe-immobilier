

// document.addEventListener('DOMContentLoaded', () => {

    const body = document.getElementsByTagName('body');
    const admin_toolbar = document.getElementById('toolbar-bar');
    const menu_admin_client = document.querySelector(".menu-admin-client");
    const btn_toggle_sidebar = document.querySelector(".toggle-sidebar");

    // body[0].dataset.menuAdminClient = 'open';

    function toggle_sidebar(elem) {
        if (!elem.classList.contains("active")) {
            elem.classList.add("active");
            body[0].dataset.menuAdminClient = 'open';
            body[0].style.transition = "padding-left .3s ease-in-out";
            admin_toolbar !== null ? admin_toolbar.style.transition = "margin-left .3s ease-in-out" : "";
            menu_admin_client.style.transition = "width .3s ease-in-out";
            localStorage.setItem('admin_sidebar', 'true')
        } else {
            elem.classList.remove("active")
            body[0].dataset.menuAdminClient = '';
            body[0].style.transition = "padding-left .3s ease-in-out";
            admin_toolbar !== null ? admin_toolbar.style.transition = "margin-left .3s ease-in-out" : "";
            localStorage.setItem('admin_sidebar', 'false');
        }
    }

    /**
     * Gestion de l'affichage de la sidebar
     */
    if (localStorage.getItem('admin_sidebar') === "false") {
        body[0].dataset.menuAdminClient = '';
        toggle_sidebar(menu_admin_client);
    } else {
        body[0].dataset.menuAdminClient = 'open';
    }

    document.querySelector(".toggle-sidebar").addEventListener('click', (e) => {
        toggle_sidebar(menu_admin_client);
    })

// });

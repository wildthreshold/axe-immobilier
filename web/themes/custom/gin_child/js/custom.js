

/**
 * tester si on se trouve ou pas dans un iframe
 * @returns {boolean}
 */
function inIframe () {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

window.addEventListener("DOMContentLoaded", (event) => {


    /**
     * Corriger les bugs de style pour la modal du navigateur de media.
     */
    if (inIframe() === true) {
        // const iframe = window.top.jQuery("#entity_browser_iframe_media_directories_modal")
        const iframe = window.top.document.getElementById("entity_browser_iframe_media_directories_modal");
        const contenu_iframe = iframe.contentDocument;
        contenu_iframe.body.style.cssText = "padding: 0!important";
        contenu_iframe.querySelector('.browser--body').style.height = "79vh";
        if (contenu_iframe.querySelector('.browser--body .views-field-thumbnail__target-id')) {
            contenu_iframe.querySelector('.browser--body .views-field-thumbnail__target-id').style.cssText = "border: inherit; border-radius: inherit";
        }
    }
});

